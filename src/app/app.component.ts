import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  searchedData: any;
  searchError: any;

  storeSearchedData(event) {
    this.searchedData = event;
  }
  storeSearchedError(event) {
    this.searchError = event;
    setTimeout(() => {
      this.searchError = null;
    }, 3000);
  }
}
