import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { SearchService } from "./search.service";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.css"]
})
export class SearchComponent implements OnInit {
  @Output() searchedData: EventEmitter<any> = new EventEmitter();
  @Output() searchError: EventEmitter<any> = new EventEmitter();

  searchedValue: string = "";
  isSearchValueEmpty: boolean = false;

  constructor(private searchService: SearchService) {}

  ngOnInit() {}

  clearSearchedValue() {
    this.searchedValue = "";
  }

  getWeatherData() {
    this.isSearchValueEmpty = false;
    if (this.searchedValue.length > 0) {
      this.searchService.getWatherDataByCityName(this.searchedValue).subscribe(
        res => {
          this.searchedData.emit(res);
          localStorage.removeItem("lastData");
          localStorage.setItem("lastData", JSON.stringify(res));
        },
        err => {
          this.searchError.emit(err);
        }
      );
    } else {
      this.isSearchValueEmpty = true;
    }
  }
}
