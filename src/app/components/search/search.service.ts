import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class SearchService {
  weatherAPI_KEY = `99710d4c29d182ebecd38bd652d59684`;
  weatherAPI = `https://api.openweathermap.org/data/2.5/weather`;

  constructor(private httpClient: HttpClient) {}

  getWatherDataByCityName(city) {
    return this.httpClient.get(
      `${this.weatherAPI}?q=${city}&appid=${this.weatherAPI_KEY}`
    );
  }
}
